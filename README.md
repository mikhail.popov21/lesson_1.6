# Lesson 1.6


## Описание задания
```sh
Напиcать docker-compose.yml со следующими условиями:
1. 3 контейнера nginx1 nginx2 grafana (https://hub.docker.com/r/grafana/grafana)
2 Nginx1 port 8001 на Grafana 
3. Nginx2 port 8002 на Grafana 
4. Grafana не пробрасывать ничего наружу 
Она не должна подниматься если вы её стопорите руками.
5. Nginx1 и Nginx2 в разных сетях.
6. И конечно 2 конфига нжинкса которые будут пробрасывать на Grafana
7. Сделать хелсчек Nginx1 в контейнере с Grafana и уронить nginx.
```



## 1. Description Docker-compose:
 В соответсвие с данным заданием необходимо "пробросить" два конфига nginx на Grafana, то есть nginx у нас будет в роли обратного прокси-сервера, и при переходе по портам 8001, и 8002 мы должны попасть в "веб-морду" Grafana. 


### 1.2. Description nginx.conf:
 ```sh
 server {
    listen 8002;
    server_name  localhost;
    location / {
        proxy_pass           http://web:3000/;
    }
}
 ```
- ```listen``` -- указывает порт подключения.
- ```server_name``` -- указывает адрес в данном случаем им выступает локальная машина.
- ```proxy_pass``` -- указывает куда будет проксироваться в данном случае контейнер Grafana в файле docker-compose определен названием web.

### 1.2. Description web-service:
```sh
web:
    image: grafana/grafana:5.4.3
    restart: always
    container_name: web
    expose:
      - "3000"
    networks:
      - nginx
      - nginx_2
    healthcheck:
      test: ["CMD", "curl", "-f", "http://nginx:8001"]
      interval: 30s
      timeout: 10s
      retries: 5
 ```

- ```restart: on-failure``` -- указывает на перезапуск контейнера в случае если он исполнился с ошибкой. 
- ```expose:3000```  -- открывает порт на 3000 порту, поскольку на данном порту будет работать приложение. 
- ```networks:``` -- создается две сети с двумя контейнера по порту 8001 и 8002 соответсвенно.
- ```healthcheck``` -- блок отвечающих за проверку работоспособности соединения, проверка осущетсвляется с помощью http запросов на nginx:8001.


```Примечание:```
Было изначальна выбрана Grafana версии latest, но по не понятным причинам с ней не работал healthcheck, и была выбрана крайняя стаблильная версия ПО.


## 2. Building and check of work
- Сборка docker-compase производится следующей командой:
```docker-compose build```
- Запуск docker-compose  производится следующей командой:
```docker-compose up``` или в фоновом режиме ```docker-compose up -d ```



 ##### Проверка работы :
- Для проверки работы необходимо выполнить команду ```curl 127.0.0.1:8001``` и ```curl 127.0.0.1:8002```   В случае если настройка выполнена корректно отрисуется страница в виде html, а в логах можно увидеть подобную запись:
```<a href="/login">Found</a>.``` 
- Проверка healthcheck:

```
docker-compose ps 
 Name                Command                  State                            Ports
----------------------------------------------------------------------------------------------------------
nginx     /docker-entrypoint.sh ngin ...   Up             80/tcp, 0.0.0.0:8001->8001/tcp,:::8001->8001/tcp
nginx_2   /docker-entrypoint.sh ngin ...   Up             80/tcp, 0.0.0.0:8002->8002/tcp,:::8002->8002/tcp
web       /run.sh                          Up (healthy)   3000/tcp
```
У сервиса ```web``` появилась подпись ```healthy```, данная запись говорит о том, хелфсчек работает. 
В случае остановки сервиса первого nginx, в логах будет следующая запись:
```
nginx      | 172.25.0.3 - - [20/Dec/2021:13:55:38 +0000] "GET / HTTP/1.1" 302 29 "-" "curl/7.52.1" "-"
nginx      | 2021/12/20 13:55:41 [notice] 1#1: signal 3 (SIGQUIT) received, shutting down
nginx      | 2021/12/20 13:55:41 [notice] 30#30: gracefully shutting down
nginx      | 2021/12/20 13:55:41 [notice] 30#30: exiting
nginx      | 2021/12/20 13:55:41 [notice] 30#30: exit
nginx      | 2021/12/20 13:55:41 [notice] 1#1: signal 17 (SIGCHLD) received from 30
nginx      | 2021/12/20 13:55:41 [notice] 1#1: worker process 30 exited with code 0
nginx      | 2021/12/20 13:55:41 [notice] 1#1: exit
nginx exited with code 0
``` 
И спустя определенное время сервис ```web``` изменит свое состояние с healty на unhealthy, а так же перестанет быть доступным по 8001 порту.



